#include <iostream>
#include <fstream>
#include <set>

#ifdef ENABLE_ADBLOCK_JSTRACER


class JSTracer {
public:
	JSTracer(std::string tracefile, std::string blacklistfile);
	~JSTracer(){
		tracelog.close();
	};
	std::set<std::string> Blacklist;
	std::ofstream tracelog;
	bool isInterestedScript(std::string scripturl);
	void writeToTrace(std::string line);
};


extern JSTracer * adblocker_jstracer;
#endif