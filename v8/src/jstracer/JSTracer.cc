#include "JSTracer.h"

#ifdef ENABLE_ADBLOCK_JSTRACER

JSTracer * adblocker_jstracer = NULL;


JSTracer::JSTracer(std::string tracefile, std::string blacklistfile)
{
	std::string line;
		//initialize the blacklist
	std::cout << "initialize the blacklist" << std::endl;
	std::ifstream blacklist(blacklistfile.c_str());
	while(blacklist.good())
	{
		getline(blacklist, line);
		Blacklist.insert(line);
		std::cout << line <<std::endl;
	}

	blacklist.close();

	std::cout << "intialize the trace file " << std::endl;

	tracelog.open(tracefile.c_str(), std::ofstream::out);

	//tracelog << "hihi" <<std::endl;


}


bool JSTracer::isInterestedScript(std::string scripturl)
{
	std::set<std::string>::iterator it;
	it = Blacklist.find(scripturl);

	if(it == Blacklist.end())
		return true;
	return false;
}


void JSTracer::writeToTrace(std::string line)
{
	tracelog << line <<std::endl;
}
#endif